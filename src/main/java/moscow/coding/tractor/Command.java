package moscow.coding.tractor;

public enum Command {
    FORWARD,
    TURNCLOCKWISE
}
