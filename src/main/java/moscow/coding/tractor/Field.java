package moscow.coding.tractor;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Field {

    @Getter
    private int fieldXlength;

    @Getter
    private int fieldYlength;

    public Field() {
        fieldXlength = 5;
        fieldYlength = 5;
    }

    @Override
    public String toString() {
        return "Field{" +
                "fieldXlength=" + fieldXlength +
                ", fieldYlength=" + fieldYlength +
                '}';
    }
}
