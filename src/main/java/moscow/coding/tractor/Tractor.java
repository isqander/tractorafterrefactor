package moscow.coding.tractor;

import ch.qos.logback.classic.Logger;
import lombok.Getter;
import org.slf4j.LoggerFactory;

import static moscow.coding.tractor.Orientation.*;

public class Tractor {
    @Getter
    private int positionX;

    @Getter
    private int positionY;

    Field field;

    @Getter
    private Orientation orientation;

    Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    public Tractor() {
        positionX = 0;
        positionY = 0;
        field = new Field();
        orientation = Orientation.NORTH;
        logger.debug("A new default tractor is burn: " + this);
    }

    public Tractor(int positionX, int positionY, Field field, Orientation orientation) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.field = field;
        this.orientation = orientation;
        logger.debug("A new custom tractor is burn: {} \n positionX: {} \n positionX: {} \n field: {} \n orientation: {}",
                this, positionX, positionY, field, orientation);
    }

    public void move(Command command) {
        switch (command) {
            case FORWARD:
                moveForwards();
                break;
            case TURNCLOCKWISE:
                turnClockwise();
                break;
        }
    }

    private void moveForwards() {
        switch (orientation) {
            case NORTH:
                positionY++;
                break;
            case EAST:
                positionX++;
                break;
            case WEST:
                positionX--;
                break;
            case SOUTH:
                positionY--;
                break;
        }
        checkDitches();
    }

    private void turnClockwise() {
        switch (orientation) {
            case NORTH:
                orientation = EAST;
                break;
            case EAST:
                orientation = SOUTH;
                break;
            case WEST:
                orientation = NORTH;
                break;
            case SOUTH:
                orientation = WEST;
                break;
        }
    }

    private void checkDitches() {
        if (positionX > field.getFieldXlength() || positionY > field.getFieldYlength() || positionX < 0 || positionY < 0) {
            try {
                throw new TractorInDitchException();
            } catch (TractorInDitchException e) {
                logger.error("You went out of borders. Take the TractorInDitchException: \n" + e.getStackTrace());
                throw e;
            }
        }
    }
}