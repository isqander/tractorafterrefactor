package moscow.coding.tractor;

public enum Orientation {
    NORTH,
    SOUTH,
    EAST,
    WEST
}
