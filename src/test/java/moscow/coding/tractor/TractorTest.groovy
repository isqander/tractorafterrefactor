package moscow.coding.tractor

import ch.qos.logback.classic.Level
import spock.lang.Specification

import static moscow.coding.tractor.Command.*

class TractorTest extends Specification {
    def "test create default + move 1"() {
        given:
        Tractor tractor = new Tractor()
        tractor.logger.setLevel(Level.DEBUG)
        when:
        tractor.move(FORWARD)
        then:
        tractor.getPositionX() == 0
        tractor.getPositionY() == 1
        tractor.field.getFieldXlength() == 5
        tractor.field.getFieldYlength() == 5
    }

    def "test create custom + move"() {
        given:
        Tractor tractor = new Tractor(3, 3, new Field(5, 8), Orientation.EAST)
        tractor.logger.setLevel(Level.DEBUG)
        when:
        tractor.move(FORWARD)
        tractor.move(FORWARD)
        tractor.move(TURNCLOCKWISE)
        tractor.move(FORWARD)
        then:
        tractor.getPositionX() == 5
        tractor.getPositionY() == 2
        tractor.getOrientation() == Orientation.SOUTH
        tractor.field.getFieldXlength() == 5
        tractor.field.getFieldYlength() == 8
    }

    def "test fall to ditch"() {
        given:
        Tractor tractor = new Tractor()
        tractor.logger.setLevel(Level.DEBUG)
        when:
        tractor.move(TURNCLOCKWISE)
        tractor.move(TURNCLOCKWISE)
        tractor.move(TURNCLOCKWISE)
        tractor.move(FORWARD)
        then:
        thrown TractorInDitchException
    }
}
